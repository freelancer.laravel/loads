export default function concatenationPoints(value) {
  if (value[0] && value[1]) {
    return value[0].name + '-' + value[1].name;
  }else {
    return value;
  }

}
