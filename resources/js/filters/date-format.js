export default function dateFormat(value) {
  var value = value.split('-');
  var year = value[0];
  var month = value[1];
  var day = value[2];

  return day + '.' + month;
}
