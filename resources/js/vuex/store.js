import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex);

const store = new Vuex.Store({
  state:{
    loads:[],
  },
  mutations:{
    SET_LOADS_TO_VUEX:(state, loads) => {
      state.loads = loads.data;
    }
  },
  actions:{
    GET_LOADS_FROM_API({commit}){
      return axios.get('http://127.0.0.1:8000/api/v1/loads')
      .then((response) => {
        commit('SET_LOADS_TO_VUEX', response.data)
      });
    }
  },
  getters:{
    LOADS(state){
      return state.loads;
    }
  }
});

export default store
