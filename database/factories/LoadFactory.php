<?php

namespace Database\Factories;

use App\Models\Load;
use Illuminate\Database\Eloquent\Factories\Factory;

class LoadFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Load::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => self::getRandomNameLoad($this->faker->numberBetween(0, 19)),
            'weight' => $this->faker->numberBetween(1, 20),
        ];
    }

    public function getRandomNameLoad($number) {

      $loads =
        [
          'Картопля', 'Горох', 'Кавуни', 'Грушi', 'Яблука',
          'Динi', 'Цибуля', 'Цукор', 'Кукурудза', 'Ячмiнь',
          'Просо', 'Гiрчиця', 'Пшениця', 'Черешня', 'Абрикос',
          'Персик', 'Баклажани', 'Гарбузи', 'Солодкий перець', 'Сливи'
        ];

        return $loads[$number] ?? $loads[0];
    }
}
