<?php

namespace Database\Factories;

use App\Models\Point;
use Illuminate\Database\Eloquent\Factories\Factory;

class PointFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Point::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }

    public function getRandomNameCity($number) {

      $cities =
        [
          'Днепр', 'Харьков', 'Одесса', 'Новомосковск', 'Киев',
          'Полтава', 'Запорожье', 'Ивано-Франковск', 'Львов', 'Казатин',
          'Орехов', 'Никополь', 'Днепро-Дзержинск', 'Ковель', 'Мариуполь',
          'Персик', 'Мелитополь', 'Херсон', 'Павлоград', 'Черновцы'
        ];

        return $cities[$number] ?? $cities[0];
    }
}
