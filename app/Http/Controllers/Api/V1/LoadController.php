<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Load;
use App\Models\Point;
use App\Http\Resources\LoadResource;
use App\Http\Requests\LoadStoreRequest;

class LoadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return LoadResource::collection(Load::orderBy('created_at', 'desc')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\LoadStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LoadStoreRequest $request)
    {
        $createdLoad = Load::create($request->validated());

        $count = 0;

        for ($i=0; $i < 2; $i++) {
          $point = new Point();
          $point->load_id = $createdLoad->id;
          $point->name = $request->input('points')[$count];
          $point->date = $request->input('dates')[$count];
          $point->save();
          $count++;
        }

        return new LoadResource($createdLoad);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
