<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoadStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dates' => 'required|array',
            'points' => 'required|array',
            'name' => 'required|string',
            'weight' => 'required|numeric',
        ];
    }
}
